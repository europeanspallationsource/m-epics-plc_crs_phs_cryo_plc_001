
# @field IPADDR
# @type STRING
# PLC IP address

# @field RECVTIMEOUT
# @type INTEGER
# PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)

# @field REQUIRE_plc_crs_phs_cryo_plc_001_VERSION
# @runtime YES

# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field REQUIRE_plc_crs_phs_cryo_plc_001_PATH
# @runtime YES

#- S7 port           : 2000
#- Input block size  : 1970 bytes
#- Output block size : 0 bytes
#- Endianness        : BigEndian
s7plcConfigure("CrS-PHS:Cryo-PLC-001", $(IPADDR), 2000, 1970, 0, 1, $(RECVTIMEOUT), 0)

#- Modbus port       : 502
drvAsynIPPortConfigure("CrS-PHS:Cryo-PLC-001", $(IPADDR):502, 0, 0, 1)

#- Link type         : TCP/IP (0)
modbusInterposeConfig("CrS-PHS:Cryo-PLC-001", 0, $(RECVTIMEOUT), 0)

#- Slave address     : 0
#- Function code     : 16 - Write Multiple Registers
#- Addressing        : Absolute (-1)
#- Data segment      : 20 words
drvModbusAsynConfigure("CrS-PHS:Cryo-PLC-001write", "CrS-PHS:Cryo-PLC-001", 0, 16, -1, 20, 0, 0, "S7-1500")

#- Slave address     : 0
#- Function code     : 3 - Read Multiple Registers
#- Addressing        : Relative (0)
#- Data segment      : 10 words
#- Polling           : 1000 msec
drvModbusAsynConfigure("CrS-PHS:Cryo-PLC-001read", "CrS-PHS:Cryo-PLC-001", 0, 3, 0, 10, 0, 1000, "S7-1500")

#- Load plc interface database
dbLoadRecords("plc_crs_phs_cryo_plc_001.db", "PLCNAME=CrS-PHS:Cryo-PLC-001, MODVERSION=$(REQUIRE_plc_crs_phs_cryo_plc_001_VERSION), S7_PORT=2000, MODBUS_PORT=502")

#- Configure autosave
#- Number of sequenced backup files to write
save_restoreSet_NumSeqFiles(1)

#- Specify directories in which to search for request files
set_requestfile_path("$(REQUIRE_plc_crs_phs_cryo_plc_001_PATH)", "misc")

#- Specify where the save files should be
set_savefile_path("$(SAVEFILE_DIR)", "")

#- Specify what save files should be restored
set_pass0_restoreFile("plc_crs_phs_cryo_plc_001.sav")

#- Create monitor set
doAfterIocInit("create_monitor_set('plc_crs_phs_cryo_plc_001.req', 1, '')")
